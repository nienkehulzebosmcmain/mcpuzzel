import { Component, HostBinding, HostListener, Input, OnInit } from "@angular/core";
import { PuzzleService } from "../puzzle/puzzle.service";
import { TilePosition } from "./tile.model";

@Component({
    selector: 'tile',
    standalone: true,
    templateUrl: 'tile.component.html',
    styleUrl: 'tile.component.css'
})
export class TileComponent implements OnInit {
    @Input({ required: true }) data!: { row: number, column: number }
    @HostBinding('class') private class: string = 'cell';
    @HostBinding('style.width.px') private width!: number;
    @HostBinding('style.height.px') private height!: number;
    @HostBinding('style.background') private background!: string;
    @HostListener('click') onClick() { this.move(); }

    public tileNumber!: number;

    constructor(private puzzleService: PuzzleService) { }

    ngOnInit(): void {
        const background = this.puzzleService.background;
        const rows = this.puzzleService.rows;
        
        this.tileNumber = this.puzzleService.calculateTileIndex(new TilePosition(this.data.row, this.data.column));
        this.width = background.width / rows;
        this.height = background.height / rows; 

        const factor = 100 / (rows - 1);
        if (this.data.row == rows - 1 && this.data.column == rows - 1) {
            this.class = 'empty';
            this.background = '';
        }
        else {
            this.background = `url(${background.src}) ${this.data.column * factor}% ${this.data.row * factor}%/${background.width + rows}px`;
        }
    }

    private move(): void {
        this.puzzleService.move(new TilePosition(this.data.row, this.data.column));
    }
}