export class TilePosition {
    constructor(public row: number, public column: number) {}

    public isEqual(check: TilePosition): boolean {
        if (this.row == check.row && this.column == check.column) {
            return true;
        }
        return false;
    }
}