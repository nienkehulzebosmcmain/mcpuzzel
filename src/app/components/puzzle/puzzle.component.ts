import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";
import { TileComponent } from "../tile/tile.component";
import { PuzzleService } from "./puzzle.service";
import { NgComponentOutlet, NgFor } from "@angular/common";
import { FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";

@Component({
    selector: 'puzzle',
    standalone: true,
    imports: [TileComponent, NgComponentOutlet, NgFor, ReactiveFormsModule],
    templateUrl: 'puzzle.component.html',
    styleUrl: 'puzzle.component.css',
    providers: [PuzzleService]
})
export class PuzzleComponent implements OnInit, AfterViewInit {
    @Input({ required: true }) imageUrl!: string;
    @Input({ required: true }) rows!: number;
    @ViewChild('wrapper') public wrapper!: ElementRef;

    public component = TileComponent;
    public finished: boolean = false;
    public optionsForm: FormGroup = new FormGroup({});

    constructor(public puzzleService: PuzzleService) { }

    ngAfterViewInit(): void {
        this.setWrapperStyle();
    }

    ngOnInit(): void {
        this.setForm();
        this.setup();
    }

    private setWrapperStyle() {
        const wrapper: HTMLDivElement = this.wrapper.nativeElement;
        wrapper.style.gridTemplateColumns = `repeat(${this.rows}, 1fr)`
        wrapper.style.width = `50${this.rows}px`
        wrapper.style.height = `50${this.rows}px`
    }

    private setForm() {
        const shuffleCountFormControl: FormControl = new FormControl(this.puzzleService.shuffleCount)
        this.optionsForm.addControl('shuffleCount', shuffleCountFormControl);
        shuffleCountFormControl.valueChanges.subscribe((value: number) => {
            this.puzzleService.shuffleCount = value;
        })
    }

    private setup() {
        this.puzzleService.finishedEvent.subscribe((value: boolean) => this.finished = value)
        this.puzzleService.setup(this.rows, this.imageUrl);
    }
}