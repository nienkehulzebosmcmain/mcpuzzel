import { EventEmitter, Injectable } from "@angular/core";
import { TilePosition } from "../tile/tile.model";

@Injectable({
    providedIn: 'root'
})
export class PuzzleService {
    private _background!: HTMLImageElement;
    public get background() {
        return this._background;
    }
    private set background(value: HTMLImageElement) {
        this._background = value;
    } 
    
    private _finishedEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    public get finishedEvent() {
        return this._finishedEvent;
    }
    
    private _shuffleCount: number = 50;
    public get shuffleCount() {
        return this._shuffleCount;
    }
    public set shuffleCount(value: number) {
        if (value > 1) {
            this._shuffleCount = value;
        }
        else {
            console.log('Shuffle count has to be bigger than 1. Changed to default value of 50')
            this._shuffleCount = 50;
        }
    }
    
    private _tiles: TilePosition[] = [];
    public get tiles() {
        return this._tiles;
    }
    private set tiles(value: TilePosition[]) {
        this._tiles = value;
    }

    private _rows!: number;
    public get rows() {
        return this._rows;
    }
    private set rows(value: number) {
        this._rows = value;
    }

    private emptyTileIndex!: number;
    
    constructor() { }
    
    public setup(rows: number, imageUrl: string) {
        this.rows = rows;
        this.emptyTileIndex = (this.rows * this.rows) - 1;

        this.background = new Image();
        this.background.src = imageUrl;
        this.background.width = 500;
        this.background.height = 500;

        for (let i = 0; i < this.rows; i++) {
            for (let j = 0; j < this.rows; j++) {
                this.tiles.push(new TilePosition(i, j));
            }
        }
        
        this.shuffle();
    }

    public newGame() {
        this.tiles.sort((a: TilePosition, b: TilePosition) => {
            if (a.isEqual(b)) {
                return 0;
            }

            const tileNumberA = this.calculateTileIndex(a);
            const tileNumberB = this.calculateTileIndex(b);
            
            if (tileNumberA > tileNumberB) {
                return 1;
            }

            return -1;
        });

        this.finishedEvent.emit(false);
    }

    public move(moveTilePosition: TilePosition) {
        const moveTileIndex: number = this.tiles.findIndex((value: TilePosition) => value.isEqual(moveTilePosition));
        if (moveTileIndex != this.emptyTileIndex) {
            if (moveTileIndex + this.rows == this.emptyTileIndex) {
                this.update(moveTileIndex);
            }

            if (moveTileIndex - this.rows == this.emptyTileIndex) {
                this.update(moveTileIndex);
            }

            if (moveTileIndex + 1 == this.emptyTileIndex) {
                this.update(moveTileIndex);
            }

            if (moveTileIndex - 1 == this.emptyTileIndex) {
                this.update(moveTileIndex);
            }

            this.checkIfFinished();
        }
    }

    public shuffle() {
        let previousMoveIndex: number = this.emptyTileIndex;
        for (let i = 0; i < this.shuffleCount; i++) {
            let move: number = this.getRandomMove(this.emptyTileIndex);
            while (move == previousMoveIndex) {
                move = this.getRandomMove(this.emptyTileIndex);
            }
            previousMoveIndex = move;
            this.update(move);
        }

        this.validateEmptyTile();

        if (this.checkIfFinished()) {
            this.shuffle();
        }
        else {
            this.finishedEvent.emit(false);
        }
    }

    public calculateTileIndex(tilePosition: TilePosition): number {
        return this.rows * tilePosition.row + (tilePosition.column + 1);
    }

    private update(moveTileIndex: number) {
        const temp: TilePosition = this.tiles[moveTileIndex];
        this.tiles[moveTileIndex] = this.tiles[this.emptyTileIndex];
        this.tiles[this.emptyTileIndex] = temp;
        this.emptyTileIndex = moveTileIndex;
    }

    private getRandomMove(moveTileIndex: number): number {
        const possibleMoves: number[] = this.getPossibleMoves(moveTileIndex);
        const randomIndex: number = Math.floor(Math.random() * possibleMoves.length)
        return possibleMoves.at(randomIndex)!;
    }

    private getPossibleMoves(move: number): number[] {
        let possibleMoves: number[] = [];

        if ((move + this.rows) < this.tiles.length) {
            possibleMoves.push(move + this.rows);
        }

        if ((move - this.rows) >= 0) {
            possibleMoves.push(move - this.rows);
        }

        if ((move - 1) >= 0) {
            if (((move - 1) % this.rows) != this.rows - 1) {
                possibleMoves.push(move - 1);
            }
        }

        if ((move + 1) < this.tiles.length) {
            if (((move + 1) % this.rows) != 0) {
                possibleMoves.push(move + 1);
            }
        }

        return possibleMoves;
    }

    private validateEmptyTile() {
        const targetPosition: number = this.tiles.length - 1;
        while (this.emptyTileIndex != targetPosition) {
            if (this.emptyTileIndex + this.rows < targetPosition) {
                this.update(this.emptyTileIndex + this.rows);
            }
            else {
                this.update(this.emptyTileIndex + 1);
            }
        }
    }

    private checkIfFinished(): boolean {
        for(let i = 0; i < this.tiles.length; i ++) {
            const index: number = this.calculateTileIndex(this.tiles[i]) - 1;
            if (index != i) {
                return false;
            }
        }
        this.finishedEvent.emit(true);
        return true;
    }
}