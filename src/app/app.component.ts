import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { PuzzleComponent } from './components/puzzle/puzzle.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, PuzzleComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  constructor() {}
}
