# Mcpuzzel
Gemaakt door Nienke :)

## Features
- Het is mogelijk om meerdere puzzles op een pagina te maken.
- Het is mogelijk om zelf een image voor de puzzel te geven.
- De shuffle is een manual shuffle. Vanaf de huidige staat van de puzzel worden er een aantal random stappen gezet. Vervolgens wordt de empty tile altijd teruggezet naar de hoek rechtsonderin. Op deze manier is de puzzel altijd oplosbaar.
